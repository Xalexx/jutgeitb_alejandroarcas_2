package model

import blueColor
import greenColor
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import menuProfileSelection
import redColor
import resetColor
import sc
import studentMenu

class Student {

    fun itineraryOfProblems(listOfProblems: List<String>){
        for (i in listOfProblems.indices){
            val problem = Json.decodeFromString<Problem>(listOfProblems[i])
            if (!problem.resolt) {
                if (Problems().buildProblem(i) == "1"){
                    Problems().playUser(i)
                } else {
                    println("\n${blueColor}\n¡Nos vemos a la proxima!$resetColor\n")
                    menuProfileSelection()
                }
            }
        }
    }

    fun consultSolvedProblems(listOfProblems: List<String>){
        println("${redColor}Estos son los problemas resueltos:$resetColor")
        val listToCheck = mutableListOf<Problem>()
        for (i in listOfProblems.indices){
            val solvedProblemObj = Json.decodeFromString<Problem>(listOfProblems[i])
            if (solvedProblemObj.resolt) {
                listToCheck.add(solvedProblemObj)
                println("${blueColor}Problema ${i+1} --> $greenColor${solvedProblemObj.intents} intento/s$resetColor")
            }
        }
        if (listToCheck.isEmpty()) println("Actualmente ${redColor}NO$resetColor hay ningun problema resuelto")
        println()
        studentMenu()
    }

    fun listOfProblems(listOfProblems: List<String>) {
        val listOfCategories = mutableListOf<Problem>()
        println()
        for (i in listOfProblems.indices) {
            val obj = Json.decodeFromString<Problem>(listOfProblems[i])
            listOfCategories.add(obj)
        }
        val categoryListSorted = listOfCategories.sortedByDescending { it.theme.length }
        for (i in categoryListSorted.indices){
            println("${blueColor}${categoryListSorted[i].theme}$resetColor")
            println("${redColor}Problema ${i+1}$resetColor${categoryListSorted[i].enunciat}\n")
        }
        println("\n${blueColor}¿Quieres resolver algun problema de los mostrados?$resetColor\nSelecciona el ${redColor}numero$resetColor del problema a resolver,")
        println("si no quieres resolver ningun problema, escribe un ${redColor}0$resetColor")
        val numberOfProblem = sc.next()
        if (numberOfProblem == "0") studentMenu()
        else {
            for (i in categoryListSorted.indices){
                if ((i+1) == numberOfProblem.toInt()) {
                    val play = Problems().buildProblem(i)
                    if (play == "1") {
                        Problems().playUser(i)
                        studentMenu()
                    }
                    else {
                        println("\n${blueColor}\n¡Nos vemos a la proxima!$resetColor\n")
                        studentMenu()
                    }
                }
            }
        }
    }
}