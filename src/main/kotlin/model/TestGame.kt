package model

import kotlinx.serialization.Serializable

@Serializable
data class TestGame(val input: String, val output: String)