package model

import kotlinx.serialization.Serializable

@Serializable
data class Problem(
    var theme: String,
    var title: String,
    var enunciat: String,
    var jocProvaPublic: List<TestGame>,
    var jocProvaPrivat: List<TestGame>,
    var resolt: Boolean,
    var intents: Int)