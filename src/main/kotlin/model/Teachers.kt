package model

import blueColor
import currentTeacherName
import greenColor
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import listOfProblems
import problemFile
import redColor
import resetColor
import sc
import teacherFile

import teacherMenu
import java.io.*

class Teachers{

    fun addTeacher(name: String, password: String){
        val oldSize = teacherFile.readLines().size
        // Add teacher to jsonFile
        val newTeacher = Teacher(name, password, null)
        val toJson = Json.encodeToString(newTeacher)
        FileOutputStream(teacherFile, true).bufferedWriter().use { writer ->
            writer.write(toJson + "\n")
        }
        val newSize = teacherFile.readLines().size
        if (newSize > oldSize) {
            println("${redColor}Docente$resetColor añadido correctamente.")
            currentTeacherName = name
        }
    }

    fun loginTeacher(): Boolean {
        var teacherExist = false
        println("\n${redColor}Inicio de sesion\n‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾\n${resetColor}Nombre completo:")
        sc.nextLine()
        val user = sc.nextLine()
        println("Contraseña:")
        val pass = sc.nextLine()
        // Find teacher
        val teacherFileLine = teacherFile.readLines()
        for (i in teacherFileLine.indices){
            val currentTeacher = Json.decodeFromString<Teacher>(teacherFileLine[i])
            if (user == currentTeacher.name && pass == currentTeacher.password) {
                teacherExist = true
                currentTeacherName = currentTeacher.name
            }
        }
        return teacherExist
    }

    fun setScoreForEachProblem(){
        // Get solved problems
        val problemReadLines = problemFile.readLines()
        for (i in problemReadLines.indices){
            val problemObj = Json.decodeFromString<Problem>(problemReadLines[i])
            if (problemObj.resolt) println("${blueColor}Problema ${i+1} --> $greenColor${problemObj.intents} intento/s")
            else println("${blueColor}Problema ${i+1} --> ${redColor}Problema no resuelto$resetColor")
        }

        println("\n${redColor}Asigna una puntuacion para los alumnos:$resetColor")
        println("Indica la ${blueColor}puntuacion$resetColor que deseas asignar:$resetColor")
        print("\nEscribe tu respuesta aqui: ")
        val problemScore = sc.nextInt()

        // Assign score to current Teacher
        pushChangesToTeacherJsonFile(currentTeacherName, problemScore)
        teacherMenu()
    }

    fun showStudentScore(){
        val fileLines = teacherFile.readLines()
        println("\n${redColor}La puntuacion total de cada profesor para los alumnos:$resetColor")
        for (i in fileLines){
            val obj = Json.decodeFromString<Teacher>(i)
            print("$greenColor${obj.name}$resetColor --> ")
            if (obj.score == null) print("${redColor}No se ha registrado ninguna puntuacion$resetColor")
            else print("$blueColor${obj.score} pt$resetColor")
            println()
        }
        println("\n")
        teacherMenu()
    }

    fun addNewProblem() {
        println("\nPara añadir un nuevo problema debes introducir la siguiente informacion")
        sc.nextLine()
        var theme :String
        do {
            println("Introduce una categoria correcta (${redColor}Tipos de datos, ${blueColor}Condicionales, ${resetColor}Bucles, ${greenColor}Listas$resetColor):")
            theme = sc.nextLine()
        } while (theme != "Tipos de datos" && theme != "Condicionales" && theme != "Bucles" && theme != "Listas")
        println("Escribe el ${redColor}titulo$resetColor:")
        val title = sc.nextLine()
        println("Escribe el ${redColor}enunciado$resetColor:")
        val enunciat = sc.nextLine()
        println("Introduce el ${redColor}numero$resetColor de juegos de prueba publicos que ${redColor}deseas$resetColor en el problema:")
        val numberOfTestGame = sc.nextInt()
        sc.nextLine()
        val listOfPublicTestGame = mutableListOf<TestGame>()
        for (i in 1..numberOfTestGame){
            println("Introduce el juego de pruebas publico:\nEscribe el ${redColor}input$resetColor del juego de prueba $i:")
            val input = sc.nextLine()
            println("Escribe el ${blueColor}output$resetColor del juego de prueba $i:")
            val output = sc.nextLine()
            listOfPublicTestGame.add(TestGame(input, output))
        }
        println("Introduce el juego de pruebas privado:\nEscribe el ${redColor}input$resetColor:")
        val input = sc.nextLine()
        println("Escribe el ${blueColor}output$resetColor:")
        val output = sc.nextLine()
        val listOfPrivateTestGame = listOf(TestGame(input, output))

        val problemObject = Problem(theme, title, "\n$enunciat", listOfPublicTestGame, listOfPrivateTestGame, false, 0)
        val objToJson = Json.encodeToString(problemObject)
        problemFile.appendText("$objToJson\n")
        listOfProblems.add(problemObject)
        println("${redColor}Problema$resetColor añadido correctamente.")
        teacherMenu()
    }

    private fun pushChangesToTeacherJsonFile(teacherName: String, score: Int) {
        val linesTeacher = teacherFile.readLines()
        teacherFile.writeText("")
        for (i in linesTeacher.indices){
            val obj = Json.decodeFromString<Teacher>(linesTeacher[i])
            if (obj.name == teacherName){
                if (obj.score == null) obj.score = score
                else obj.score = obj.score!! + score
            }
            val objModified = Json.encodeToString(obj)
            teacherFile.appendText("$objModified\n")
            val fileWriter = FileWriter(teacherFile, true)
            fileWriter.close()
        }
    }

}