package model

import kotlinx.serialization.Serializable

@Serializable
data class Teacher(var name: String, var password: String, var score: Int?)