package model

import blueColor
import greenColor
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import listOfProblems
import problemFile
import redColor
import resetColor
import sc

class Problems {

    init {
        storeProblems()
    }

    fun buildProblem(i: Int): String {
        var wantToPlay: String
        println("${blueColor}Problema ${i+1}$resetColor: ${listOfProblems[i].title}\n${listOfProblems[i].enunciat}\n")
        println("${blueColor}Juego publico:$resetColor")
        getInfoJocProva(listOfProblems[i].jocProvaPublic)
        println("\n${redColor}Quieres resolver el problema?\n${blueColor}1 $resetColor-> ${greenColor}Si    ${blueColor}2 $resetColor-> ${greenColor}No$resetColor")
        wantToPlay = ""
        while (wantToPlay != "1" && wantToPlay != "2"){
            print("Introduce ${redColor}una respuesta$resetColor valida: ")
            wantToPlay = sc.next()
        }
        return wantToPlay
    }

    fun playUser(i: Int) {
        var userAnswer: String
        var intents = 0
        println("\n${blueColor}Juego privado:$resetColor\nEntrada: ${listOfProblems[i].jocProvaPrivat[0].input}    Salida: ...")
        while (true){
            print("Introduce la repuesta correcta: ")
            userAnswer = sc.next()
            intents++
            if (userAnswer == listOfProblems[i].jocProvaPrivat[0].output) break
            else println("${redColor}Respuesta incorrecta$resetColor.\n")
        }
        println("\n${greenColor}Enhorabuena, has acertado la respuesta correcta!$resetColor\n")
        pushChangesToProblemsJsonFile(listOfProblems[i].title, intents)
    }

    private fun storeProblems(){
        val problemFileLines = problemFile.readLines()
        for (i in problemFileLines.indices){
            val obj = Json.decodeFromString<Problem>(problemFileLines[i])
            listOfProblems.add(obj)
        }
    }

    private fun getInfoJocProva(public: List<TestGame>){
        for (i in public.indices){
            print("Entrada: ${public[i].input}    Salida: ${public[i].output}\n")
        }
    }

    private fun pushChangesToProblemsJsonFile(title: String, intents: Int){
        val problemFileLines = problemFile.readLines()
        problemFile.writeText("")
        for (i in problemFileLines.indices){
            val obj = Json.decodeFromString<Problem>(problemFileLines[i])
            if (obj.title == title){
                obj.resolt = true
                obj.intents = intents
            }
            val objModified = Json.encodeToString(obj)
            problemFile.appendText("$objModified\n")
        }
    }
}