
import model.*
import java.io.File
import java.util.*
import kotlin.system.exitProcess

/* Colors Initialization */
const val redColor = "\u001b[31m"
const val blueColor = "\u001B[34m"
const val greenColor = "\u001B[32m"
const val resetColor = "\u001b[0m"

/* Global variables */
val sc = Scanner(System.`in`)
var currentTeacherName = ""

/* Files Initialization */
val listOfProblems = mutableListOf<Problem>()
val teacherFile = File("./files/teachers.json")
val problemFile = File("./files/problems.json")

fun main() {
    Problems()
    println("Bienvenido a$redColor JutgeITB 2.0$resetColor")
    println("\nSelecciona$redColor UNA$resetColor de las siguientes opciones:")
    menuProfileSelection()
}

fun menuProfileSelection() {
    println("Indica el tipo de perfil con el que deseas acceder:")
    println("${blueColor}1$resetColor ->$greenColor Alumno/a\n" +
            "${blueColor}2$resetColor ->$greenColor Docente\n" +
            "${blueColor}3$resetColor ->$greenColor Ayuda\n" +
            "${blueColor}4$resetColor ->$greenColor Salir de la aplicacion$resetColor")
    print("Introduce aqui tu respuesta: ")
    when (sc.next()){
        "1" -> {
            println("\nSelecciona$redColor UNA$resetColor de las siguientes opciones:")
            studentMenu()
        }
        "2" -> {
            println("\nSelecciona$redColor UNA$resetColor de las siguientes opciones:")
            teacherLoginType()
        }
        "3" -> helpMenu()
        "4" -> exitProcess(0)
        else -> {
            println("\nSelecciona$redColor UNA$resetColor opcion valida:")
            menuProfileSelection()
        }
    }
}


fun studentMenu(){
        println("${blueColor}1$resetColor ->$greenColor Seguir con el itinerario de problemas\n" +
                "${blueColor}2$resetColor ->$greenColor Mostrar la lista de problemas\n" +
                "${blueColor}3$resetColor ->$greenColor Mostrar el historico de problemas resueltos\n" +
                "${blueColor}4$resetColor ->$greenColor Atras$resetColor")
        print("Introduce aqui tu respuesta: ")
    when (sc.next()){
        "1" -> Student().itineraryOfProblems(problemFile.readLines())
        "2" -> Student().listOfProblems(problemFile.readLines())
        "3" -> Student().consultSolvedProblems(problemFile.readLines())
        "4" -> menuProfileSelection()
        else -> {
            println("\nSelecciona$redColor UNA$resetColor opcion valida:")
            studentMenu()
        }
    }
}


fun teacherLoginType() {
    var userMenuSelection: String
    userMenuSelection = ""
    while (userMenuSelection !in "1".."3"){
        println("${blueColor}1$resetColor ->$greenColor Inicia sesion como docente\n" +
                "${blueColor}2$resetColor ->$greenColor Registrate como docente\n" +
                "${blueColor}3$resetColor ->$greenColor Atras$resetColor")
        print("Introduce aqui tu respuesta: ")
        userMenuSelection = sc.next()
        if (userMenuSelection !in "1".."3") println("\nSelecciona$redColor UNA$resetColor opcion valida:")
    }
    var teacherExist = false
    when (userMenuSelection){
        "1" -> {
            if (Teachers().loginTeacher()) teacherExist = true
            else {
                println("${redColor}No existe un docente con ese nombre y contraseña$resetColor")
                menuProfileSelection()
            }
        }
        "2" -> {
            println("\n${redColor}Registrar un nuevo docente\n‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾\n${resetColor}Nombre completo:")
            sc.nextLine()
            val name = sc.nextLine()
            println("Contraseña:")
            val password = sc.nextLine()
            Teachers().addTeacher(name, password)
            teacherExist = true
        }
        "3" -> menuProfileSelection()
    }
    if (teacherExist){
        println("\n${greenColor}Bienvenido/a $blueColor$currentTeacherName$resetColor")
        teacherMenu()
    }
}

fun teacherMenu() {
    println("${blueColor}1$resetColor ->$greenColor Añadir un nuevo problema\n" +
            "${blueColor}2$resetColor ->$greenColor Reportar las taeras de los alumnos\n" +
            "${blueColor}3$resetColor ->$greenColor Mostrar la puntuacion de cada docente a los alumnos\n" +
            "${blueColor}4$resetColor ->$greenColor Atras$resetColor")
    print("Introduce aqui tu respuesta: ")
    when (sc.next()){
        "1" -> Teachers().addNewProblem()
        "2" -> Teachers().setScoreForEachProblem()
        "3" -> Teachers().showStudentScore()
        "4" -> teacherLoginType()
        else -> {
            println("\nSelecciona$redColor UNA$resetColor opcion valida:")
            teacherMenu()
        }
    }
}

fun helpMenu(){
    println("\n${redColor}Bienvenido al menu de ayuda de JutgeITB 2.0$resetColor")
    println("Para utilizar este programa tenemos dos tipos de perfiles,\n" +
            "${blueColor}Alumno/a$resetColor o ${blueColor}Docente$resetColor " +
            "Si accedes como ${blueColor}Alumno/a$resetColor tendras la posibilidad de\n" +
            "iniciar un itinerario de problemas, de todo tipo, una vez hayas finalizado\n" +
            "un problema, tendras la correcion inmediata y ademas tendras un historico de\n" +
            "tus problemas resueltos y con sus intentos correspondientes.\n" +
            "Por otra parte tenemos el perfil de ${blueColor}Docente$resetColor donde tenemos\n" +
            "la posibilidad de sacar una puntuacion a los alumnos ya sea por los problemas\n" +
            "resueltos o por el numero de intentos, tambien esta la posibilidad de agregar\n" +
            "mas problemas para los ${blueColor}alumnos$resetColor y ponerlo un poquito mas complicado!!")

    println("${redColor}Volver atras:$resetColor")
    println("${blueColor}1$resetColor ->$greenColor Si\n" +
            "${blueColor}2$resetColor ->$greenColor No, salir de la aplicacion$resetColor")
    when (sc.next()){
        "1" -> menuProfileSelection()
        "2" -> exitProcess(0)
        else -> {
            println("\nSelecciona$redColor UNA$resetColor opcion valida:")
            helpMenu()
        }
    }
}